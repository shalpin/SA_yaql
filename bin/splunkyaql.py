#!/usr/bin/env python
"Streaming command for yaql queries"

import collections
import sys
import os
from configparser import ConfigParser
import io

sys.path.insert(0, os.path.join(os.path.dirname(__file__), "..",
    'lib', str(sys.version_info.major)))

from splunklib.searchcommands import \
    dispatch, StreamingCommand, Configuration, Option, validators
from splunklib.searchcommands import splunklib_logger as logger
import yaql
import yaml
import xmltodict

DEFAULT_QUERY = "$"

@Configuration()
class yaqlCommand(StreamingCommand):
    """ Parse structured data (YAML,JSON, XML, .conf) using yaql (https://yaql.readthedocs.io/en/latest/).

    ##Syntax

    .. code-block::
        ... | yaql [input_field=<inputfield name>] [output_field=<outputfield_name>] [input_type=yaml|json|xml|conf] query=<yaql query>|<yaql_query>"

    ##Description

    Search the input field, using the given yaql query, return the results

    """
    query = Option(
        doc='''
        **Syntax:** **query=***<yaql_query>*
        **Description:** YAQL query. Default="$"'''
    )

    input_field = Option(
        doc='''
        **Syntax:** **input_field=***<fieldname>*
        **Description:** Name of the field that will hold the input. Default=_raw''',
        default="_raw",
        validate=validators.Fieldname()
    )

    output_field = Option(
        doc='''
        **Syntax:** **output_field=***<fieldname>*
        **Description:** Name of the field that will hold the output. Default=yaql_out''',
        default="yaql_out",
        validate=validators.Fieldname()
    )

    input_type = Option(
        doc='''
        **Syntax:** **input_type=***<input_type>*
        **Description:** The type of input (yaml, json, xml, or conf)''',
        validate=validators.OptionName(),
        default="yaml"
    )

    def __init__(self):
        "Setup the yaql engine and define available parsers"
        super(StreamingCommand, self).__init__()
        self.engine = yaql.factory.YaqlFactory().create()
        self.supported_input_types = ('yaml', 'xml', 'conf', 'json')
        self.parser = dict(yaml = yaml.safe_load,
                           xml = xmltodict.parse,
                           conf = self.config_read_string,
                           json = yaml.safe_load)

    def config_read_string(self, mystring):
        'Parse a config file, return the resulting sructure'
        if not hasattr(self, "config_parser"):
            self.config_parser = ConfigParser()
            self.config_parser.optionxform = str
        self.config_parser.readfp(io.BytesIO(mystring))
        return self.config_parser._sections

    def get_query(self):
        'Get the query from the query parameter, the first field, or a default'
        if hasattr(self, "query") and self.query:
            query=self.query
        else:
            if len(self.fieldnames) > 0:
                query = self.fieldnames[0]
            else:
                query=DEFAULT_QUERY
        return query

    def process_record(self, record):
        'For each input event, parse the query and return the resultsing event'
        input_field = self.input_field
        self.logger.debug('input_field: ', input_field)
        if not record.get(input_field):
            self.logger.debug('input_field not in record')
            return record
        input_type = self.input_type
        self.logger.debug('input_type: %s', input_type)
        if input_type not in self.supported_input_types:
            raise ValueError("Unrecognized input type %s. Supported types: %s" %
                            (input_type, ', '.join(self.supported_input_types)))
        query = self.get_query()
        self.logger.debug('yaql query: ', query)
        output_field = self.output_field
        self.logger.debug('output_field: %s', output_field)
        data_source = self.parser[input_type](record.get(input_field))
        expression = self.engine(query)
        output = expression.evaluate(data=data_source)
        record[output_field] = output
        self.logger.debug('output: ', output)
        return record

    def stream(self, records):
        'Get the yaql query and run it against the input'
        self.logger.debug('yaqlCommand: ', self)

        for record in records:
            yield  self.process_record(record)


dispatch(yaqlCommand, sys.argv, sys.stdin, sys.stdout, __name__)
