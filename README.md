# SA-yaql

## Purpose
This app searches structued data (e.g. YAML, JSON, XML, CONF) using yaql queries (https://yaql.readthedocs.io).

## License
SA_yaql is icensed under the MIT license: see license.md. For included software, refer to the site's documentation below.

## Included Software
Includes an icon from feather (https://github.com/feathericons/feather#feather) under the MIT license.

Includes:
YAQL: https://github.com/openstack/yaql
pYaml: https://pyyaml.org/ 
splunklib: https://github.com/splunk/splunk-sdk-python
xmltodict: https://github.com/martinblech/xmltodict
